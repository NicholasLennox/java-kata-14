package utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ColourUtilTest {

    ColourUtil colourUtil = new ColourUtil();

    @Test
    void testLowerLimRgb(){
        System.out.println("rgb lowest valid numbers");
        assertEquals(true, colourUtil.validRgbaColour("rgb(0,0,0)"));
    }

    @Test
    void testUpperLimRgb(){
        System.out.println("rgb highest valid numbers");
        assertEquals(true, colourUtil.validRgbaColour("rgb(255,255,255)"));
    }

    @Test
    void testLowerLimRgba(){
        System.out.println("rgba lowest valid numbers");
        assertEquals(true, colourUtil.validRgbaColour("rgba(0,0,0,0)"));
    }

    @Test
    void testUpperLimRgba(){
        System.out.println("rgba highest valid numbers");
        assertEquals(true, colourUtil.validRgbaColour("rgba(255,255,255,1)"));
    }

    @Test
    void testMulitpleDecimalsRgba(){
        System.out.println("alpha can have many decimals");
        assertEquals(true, colourUtil.validRgbaColour("rgba(0,0,0,0.123456789)"));
    }

    @Test
    void testOptionalNumberInAplha(){
        System.out.println("in alpha the number before the dot is optional");
        assertEquals(true, colourUtil.validRgbaColour("rgba(0,0,0,.8)"));
    }

    @Test
    void testWhiteSpaces(){
        System.out.println("whitespace is allowed around numbers (even tabs)");
        assertEquals(true, colourUtil.validRgbaColour("rgba(	0 , 127	, 255 , 0.1	)"));
    }

    @Test
    void testMissingNumbers(){
        System.out.println("INVALID: missing number");
        assertEquals(false, colourUtil.validRgbaColour("rgb(0,,0)"));
    }

    @Test
    void testSpaceAfterRgb(){
        System.out.println("INVALID: whitespace before parenthesis");
        assertEquals(false, colourUtil.validRgbaColour("rgb (0,0,0)"));
    }

    @Test
    void testRgbWith4Numbers(){
        System.out.println("INVALID: rgb with 4 numbers");
        assertEquals(false, colourUtil.validRgbaColour("rgb(0,0,0,0)"));
    }

    @Test
    void testRgbaWith3Numbers(){
        System.out.println("INVALID: rgba with 3 numbers");
        assertEquals(false, colourUtil.validRgbaColour("rgba(0,0,0)"));
    }

    @Test
    void testNumbersBelow0(){
        System.out.println("INVALID: numbers below 0");
        assertEquals(false, colourUtil.validRgbaColour("rgb(-1,0,0)"));
    }

    @Test
    void NumbersAbove255(){
        System.out.println("INVALID: numbers above 255");
        assertEquals(false, colourUtil.validRgbaColour("rgb(255,256,255)"));
    }

    @Test
    void testAlphaBelow0(){
        System.out.println("INVALID: alpha below 0");
        assertEquals(false, colourUtil.validRgbaColour("rgba(0,0,0,-1)"));
    }

    @Test
    void testAlphaAbove1(){
        System.out.println("INVALID: alpha above 1");
        assertEquals(false, colourUtil.validRgbaColour("rgba(0,0,0,1.1)"));
    }

    @Test
    void testNumbersAsPercentages(){
        System.out.println("numbers can be percentages");
        assertEquals(true, colourUtil.validRgbaColour("rgb(0%,50%,100%)"));
    }

    @Test
    void testOver100Percent(){
        System.out.println("INVALID: numbers above 100%");
        assertEquals(false, colourUtil.validRgbaColour("rgb(100%,100%,101%)"));
    }

}