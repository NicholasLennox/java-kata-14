package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ColourUtil {

    public boolean validRgbaColour(String input){
        boolean valid = false;
        // Get first 4 characters
        String first4 = input.substring(0,4);
        // Get whats between the brackets, aka the values
        String betweenBrackets = "";
        Matcher m = Pattern.compile("\\(([^)]+)\\)").matcher(input);
        while(m.find()) {
            betweenBrackets = m.group(1);
        }
        // Could also get between brackets with str.substring(str.indexOf("(")+1,str.indexOf(")"));

        // Check for %, and set flag, then remove so it can still be converted
        Matcher mPerc = Pattern.compile("\\d+(?:\\.\\d+)?%").matcher(betweenBrackets);
        boolean percentages = false;
        while(mPerc.find()) {
            percentages = true;
            betweenBrackets = betweenBrackets.replaceAll("%", "");
            break;
        }

        // try convert betweenBrackets to double array
        String[] betweenArray = betweenBrackets.split(",");
        double[] betweenDoubleArray = new double[betweenArray.length];
        try{
            for (int i = 0; i < betweenArray.length; i++) {
                betweenDoubleArray[i] = Double.parseDouble(betweenArray[i]);
            }
        } catch(Exception e){
            return false;
        }

        // Check first 4, caters for space, rbg (, that would be rgb*space*
        if(first4.equals("rgba")){
            valid = checkValidRgba(betweenDoubleArray);
        } else if(first4.equals("rgb(")){
            if(percentages){
                valid = checkValidRgbPercentage(betweenDoubleArray);
            } else {
                valid = checkValidRgb(betweenDoubleArray);
            }

        }
        return valid;
    }

    private boolean checkValidRgbPercentage(double[] numbers) {
        if(numbers.length != 3){
            return false;
        }
        for (int i = 0; i < 3; i++) {
            if(numbers[i] < 0 || numbers[i] > 100){
                return false;
            }
        }
        return true;
    }

    // Needs to be length 4, with alpha being between 0-1, the rest between 0-255
    private boolean checkValidRgba(double[] numbers){
        if(numbers.length != 4){
            return false;
        }
        if(numbers[3] > 1.0 || numbers[3] < 0){
            return false;
        }
        for (int i = 0; i < 3; i++) {
            if(numbers[i] < 0 || numbers[i] > 255){
                return false;
            }
        }
        return true;
    }

    // Needs to be length 3, the numbers between 0-255
    private boolean checkValidRgb(double[] numbers){
        if(numbers.length != 3){
            return false;
        }
        for (int i = 0; i < 3; i++) {
            if(numbers[i] < 0 || numbers[i] > 255){
                return false;
            }
        }
        return true;
    }
}
